<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190516062015 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE baggage_livraison');
        $this->addSql('ALTER TABLE commande_restaurant ADD livreur_id INT DEFAULT NULL, ADD date_reservation DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE commande_restaurant ADD CONSTRAINT FK_73573ED9F8646701 FOREIGN KEY (livreur_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_73573ED9F8646701 ON commande_restaurant (livreur_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE baggage_livraison (id INT AUTO_INCREMENT NOT NULL, commande_restaurant_id INT NOT NULL, user_id INT NOT NULL, date_reservation DATETIME NOT NULL, INDEX IDX_6F06AF20A76ED395 (user_id), INDEX IDX_6F06AF2050D314D6 (commande_restaurant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE baggage_livraison ADD CONSTRAINT FK_6F06AF2050D314D6 FOREIGN KEY (commande_restaurant_id) REFERENCES commande_restaurant (id)');
        $this->addSql('ALTER TABLE baggage_livraison ADD CONSTRAINT FK_6F06AF20A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commande_restaurant DROP FOREIGN KEY FK_73573ED9F8646701');
        $this->addSql('DROP INDEX IDX_73573ED9F8646701 ON commande_restaurant');
        $this->addSql('ALTER TABLE commande_restaurant DROP livreur_id, DROP date_reservation');
    }
}
