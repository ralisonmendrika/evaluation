<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 22:27
 */

namespace App\Service;


use App\Entity\CommandeRestaurant;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Integer;

class RestaurateurService
{
    private $em  ;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em ;
    }

    public function commandReady(CommandeRestaurant $commandeRestaurant) {
        $commandeRestaurant->setEtat(CommandeRestaurant::PRET) ;
        $commandeRestaurant->setDatePret(new \DateTimeImmutable())  ;
        $this->em->flush();
    }

    public function formatterReponse ($reponseDatabase) {
        $response  = [
            "cols"=>[
                ['id'=>"",'label'=>"RESTAURANT",'pattern'=>"",'type'=>"string"],
                ['id'=>"",'label'=>"CHIFFRE D'AFFAIRE",'pattern'=>"",'type'=>"number"]
            ]
        ] ;
        $row = [] ;
        foreach ($reponseDatabase as $r) {
            $row[] = ["c"=>[["v"=>$r['nom'],"f"=>null],["v"=>(int)$r['chiffre'],"f"=>null]]] ;
        }
        $response["rows"] = $row ;
        return $response ;
    }

    public function formatterEvolution($reponseDatabase) {
        $response  = [
            "cols"=>[
                ['id'=>"",'label'=>"DATE",'pattern'=>"",'type'=>"string"],
                ['id'=>"",'label'=>"COMMANDE",'pattern'=>"",'type'=>"number"]
            ]
        ] ;
        $row = [] ;
        foreach ($reponseDatabase as $r) {
            $row[] = ["c"=>[["v"=>$r["jour"]."/".$r["mois"]."/".$r["an"],"f"=>null],["v"=>(int)$r["nombreCommande"],"f"=>null]]] ;
        }
        $response["rows"] = $row ;
        return $response  ;
    }
}