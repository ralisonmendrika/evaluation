<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 16/05/2019
 * Time: 19:51
 */

namespace App\Controller\rest;


use App\Repository\CommandeRepository;
use App\Repository\CommandeRestaurantRepository;
use App\Service\RestaurateurService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;


class ApiController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/test.json")
     */
    public function index(CommandeRepository $commandeRepository){
        $findal = $commandeRepository->findAll();
        return $this->view($findal[0]->getId(),200) ;
    }

    /**
     * @Rest\Get("/restaurant.json")
     */
    public function getChiffreAffaire(CommandeRestaurantRepository $commandeRestaurantRepository,RestaurateurService $restaurateurService) {
        $restaurant = $commandeRestaurantRepository->findTopRestaurant() ;
        $response = $restaurateurService->formatterReponse($restaurant);
        return $this->view($response,200) ;
    }

    /**
     * @Rest\Get("/evolution.json")
     */
    public function evolution (CommandeRestaurantRepository $commandeRestaurantRepository,RestaurateurService $restaurateurService) {

        $evolutions  = $commandeRestaurantRepository->findEvolutionCommande() ;
        $response    = $restaurateurService->formatterEvolution($evolutions);
        return $this->view($response,200);


    }

}