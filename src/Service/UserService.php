<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 03:07
 */

namespace App\Service;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $em  ;
    private $encoder;

    public function __construct(ObjectManager $objectManager,UserPasswordEncoderInterface $encoder)
    {
        $this->em = $objectManager ;
        $this->encoder = $encoder ;
    }

    public function createUser (User $user) {
        // 3) Encode the password (you could also do this via Doctrine listener)
        $p = $this->em->getRepository(User::class )->findOneBy(['email'=>$user->getEmail()]) ;
        if($p) {
            return false ;
        }
        $password = $this->encoder->encodePassword($user, $user->getPlainPassword());
        $user->addRoles($user->getRole())  ;
        $user->setPassword($password);
        $user->setIsActif(false) ;
        $user->setCleActivation(md5($user->getEmail())) ;
        // 4) save the User!
        $this->em->persist($user);
        $this->em->flush();
        return true;
    }

}