<?php

namespace App\Form;

use App\Entity\Plat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prix')
            ->add('tagsText',TextType::class,['label'=>'Categories (Séparé par un ",")'])
            ->add('temps')
            ->add('description')
            ->addEventListener(FormEvents::PRE_SET_DATA,function (FormEvent $formEvent){
                $plat = $formEvent->getData() ;
                $form = $formEvent->getForm() ;
                if(!$plat || null == $plat->getId()){
                    $form->add('photo',PhotoType::class) ;
                }
            })


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Plat::class,
        ]);
    }
}
