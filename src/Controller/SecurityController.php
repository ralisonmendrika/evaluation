<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\MailingService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/register", name="user_registration")
     */
    public function register(Request $request,MailingService $mailingService,UserService $userService)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if(!$userService->createUser($user)){
                $this->addFlash('type','danger') ;
                $this->addFlash('message','Cet email existe déja') ;
                return $this->redirectToRoute('user_registration') ;
            }

            $mailingService->sendWelcomeEmail($user->getEmail(),$this->renderView('security/welcome.html.twig',['activation'=>$user->getCleActivation()])) ;
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('type','success');
            $this->addFlash('message','Un email a été envoyé pour confirmer votre inscription') ;

            return $this->redirectToRoute('user_registration');
        }

        return $this->render(
            'security/register.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/activate/{activation}", name="user_activation")
     */
    public function activate ($activation)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['cleActivation'=>$activation]) ;
        if(!$user) {
            throw $this->createNotFoundException('Page Introuvable') ;
        }
        $user->setIsActif(true)  ;
        $user->setCleActivation(null) ;
        $em = $this->getDoctrine()->getManager() ;
        $em->flush() ;
        $this->addFlash('type','success');
        $this->addFlash('message','Enregistrement confirmé') ;
        return $this->redirectToRoute('app_login') ;
    }

}
