<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 02:42
 */

namespace App\Service;


class MailingService
{

    private $mailer ;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer ;
    }

    public function sendWelcomeEmail($recipient,String $view)
    {

        $message = (new \Swift_Message('Inscription réussie'))
            ->setFrom('ralisonmendrika@gmail.com')
            ->setTo($recipient)
            ->setBody(
                $view,
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    ['name' => $name]
                ),
                'text/plain'
            )
            */
        ;

        $this->mailer->send($message);

    }
}