<?php

namespace App\Controller;

use App\Entity\CommandeRestaurant;
use App\Entity\DetailCommande;
use App\Repository\CommandeRestaurantRepository;
use App\Repository\DetailCommandeRepository;
use App\Service\PanierService;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client")
 */
class CommandeController extends AbstractController
{
    /**
     * @Route("/commande", name="client_commande")
     */
    public function index(PanierService $panierService,DetailCommandeRepository $detailCommandeRepository)
    {
        //$panierService->getOrCreatePanier() ;
        $commande = $panierService->getOrCreatePanier();
        //$commande->getDetailCommandes()
        $detailsCommandes = $detailCommandeRepository->findDetailCommande($commande) ;

        return $this->render('commande/index.html.twig', [
            'commande'=>$commande,
            'detailCommandes'=>$detailsCommandes
        ]);
    }

    /**
     * @Route("/delete/detailcommande/{idDetail}", name="client_delete_commande")
     */
    public function deleteDetail($idDetail,PanierService $panierService) {
        $panierService->deleteDetailCommande($idDetail) ;
        return $this->redirectToRoute('client_commande') ;
    }

    /**
     * @Route("/valider/commande",name="client_valider_commande")
     */
    public function validerCommande(PanierService $panierService) {
        $panierService->validerCommande() ;
        $this->addFlash('type','success') ;
        $this->addFlash('message','Commande validée') ;
        return $this->redirectToRoute('client_commande') ;
    }

    /**
     * @Route("/suivi/commande",name="client_suivi_commande")
     */
    public function suivreCommande(Request $request,PaginatorInterface $paginator,CommandeRestaurantRepository $commandeRestaurantRepository){
        $commandes = $paginator->paginate(
            $commandeRestaurantRepository->getCommandeClient($this->getUser()) ,
            $request->query->getInt('page',1),
            5
        );
        return $this->render('commande/commandes.html.twig',
            ['commandes'=>$commandes]) ;
    }

    /**
     * @Route("/recuperer/commande/{idCommande}",name="client_recuperer_commande")
     */
    public function recuperer($idCommande,PanierService $panierService) {
        $commande = $this->getDoctrine()->getRepository(CommandeRestaurant::class)->find($idCommande) ;
        if(!$commande){
            throw $this->createNotFoundException('Commande Introuvable') ;
        }
        if($panierService->recuperer($commande)) {
            $this->addFlash('type','success') ;
            $this->addFlash('messsage','Commande livrée, Merci') ;
        }else {
            $this->addFlash('type','danger') ;
            $this->addFlash('messsage','Livraison impossible') ;
        }
        return $this->redirectToRoute('client_suivi_commande') ;
    }

    /**
     * @Route("/creerpdf",name="client_creer_pdf")
     */
    public function genererPdf(){
        $dompdf = new Dompdf() ;
        $html   = $this->renderView("commande/invoicepdf.html.twig")  ;
        $dompdf->setPaper('A6','Landscape') ;
        $dompdf->loadHtml($html) ;
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/testChart",name="test_chart")
     */
    public function testChart(){
        return $this->render('commande/test.html.twig') ;
    }


}
