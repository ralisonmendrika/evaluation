<?php

namespace App\Controller;

use App\Entity\CommandeRestaurant;
use App\Entity\Plat;
use App\Entity\Restaurant;
use App\Form\PlatType;
use App\Form\RestaurantType;
use App\Repository\CommandeRestaurantRepository;
use App\Repository\PlatRepository;
use App\Service\RestaurateurService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/restaurateur")
 */
class PlatController extends AbstractController
{
    /**
     * @Route("/", name="plat_index", methods={"GET"})
     */
    public function index(Request $request,PlatRepository $platRepository,PaginatorInterface $paginator): Response
    {
        $plats = $paginator->paginate(
            $platRepository->getQueryPlatByRestaurant($this->getUser()->getRestaurant()),
            $request->query->getInt('page',1),
            4
        );
        return $this->render('plat/index.html.twig', [
            'plats' => $plats,
        ]);
    }

    /**
     * @Route("/plat/new", name="plat_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $plat = new Plat();
        $form = $this->createForm(PlatType::class, $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->denyAccessUnlessGranted('ROLE_RESTAURATEUR') ;
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($plat);
            $plat->setRestaurant($this->getUser()->getRestaurant()) ;
            $entityManager->flush();
            return $this->redirectToRoute('plat_index');
        }

        return $this->render('plat/new.html.twig', [
            'plat' => $plat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="plat_show", methods={"GET"})
     */
    public function show(Plat $plat): Response
    {
        return $this->render('plat/show.html.twig', [
            'plat' => $plat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="plat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Plat $plat): Response
    {
        $form = $this->createForm(PlatType::class, $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('plat_index', [
                'id' => $plat->getId(),
            ]);
        }

        return $this->render('plat/edit.html.twig', [
            'plat' => $plat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="plat_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Plat $plat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$plat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($plat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('plat_index');
    }

    /**
     * @Route("/commande/encours",name="restaurateur_commande_en_cours")
     */
    public function getCommandeEncours(PaginatorInterface $paginator,Request $request,CommandeRestaurantRepository $commandeRestaurantRepository) {
        $this->denyAccessUnlessGranted("ROLE_RESTAURATEUR") ;
        $restaurant = $this->getUser()->getRestaurant() ;
        if(!$restaurant) {
            // Formulaire Restaurant ;
        }
        $query = $commandeRestaurantRepository->getQueryCommande(CommandeRestaurant::VALIDE,$restaurant) ;
        $detailC = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            5
        )  ;

        return $this->render('admin/index.html.twig',[
            'commandes'=>$detailC,
            'content'=>'admin/_cours.html.twig',
            'title'=>'Commandes en cours'
        ]);

    }

    /**
     * @Route("/commande/pret/{idCommandeRestaurant}",name="restaurateur_plat_pret")
     */
    public function commandePret($idCommandeRestaurant,RestaurateurService $restaurateurService) {
        $commandeRestaurant = $this->getDoctrine()->getRepository(CommandeRestaurant::class)->find($idCommandeRestaurant) ;
        if(!$commandeRestaurant) {
            throw $this->createNotFoundException('Commande Introuvable') ;
        }
        $restaurateurService->commandReady($commandeRestaurant);
        $this->addFlash('type','success') ;
        $this->addFlash('message','Commande en attente de livreur');
        return $this->redirectToRoute('restaurateur_commande_en_cours')  ;
    }

    /**
     * @Route("creer/restaurant",name="restaurateur_creer_restaurant")
     */
    public function createRestaurant(Request $request) {
        $restaurant = new Restaurant() ;
        $restaurant->setResponsable($this->getUser())  ;
        $form       = $this->createForm(RestaurantType::class,$restaurant) ;
        $form->handleRequest($request)  ;
        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($restaurant) ;
            return $this->redirectToRoute('plat_new') ;
        }
        return $this->render('plat/restaurant.html.twig',['form'=>$form->createView()]);
    }
}
