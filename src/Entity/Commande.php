<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandeRepository")
 */
class Commande
{
    const VALIDE = 1 ;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailCommande", mappedBy="commande",cascade={"persist"})
     */
    private $detailCommandes;



    /**
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommandeRestaurant", mappedBy="commande",cascade={"persist"})
     */
    private $commandeRestaurants;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCommande;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=2)
     */
    private $somme;



    public function __construct()
    {
        $this->detailCommandes = new ArrayCollection();
        $this->commandeRestaurants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|DetailCommande[]
     */
    public function getDetailCommandes(): Collection
    {
        return $this->detailCommandes;
    }



    public function addSomme($value) {
        $this->somme = $this->somme + $value ;
    }

    public function subSomme($value) {
        $this->somme = $this->somme - $value ;
    }

    public function addDetailCommande(DetailCommande $detailCommande): self
    {
        foreach ($this->detailCommandes as $d) {
            if($d->getPlat()->getId() == $detailCommande->getPlat()->getId()) {
                $d->setNombre($d->getNombre()+$detailCommande->getNombre()) ;
                $this->addSomme($detailCommande->getSomme()) ;
                return $this ;
            }
        }
        $detailCommande->setCommande($this) ;
        $this->detailCommandes[] = $detailCommande ;
        $this->addSomme($detailCommande->getSomme()) ;


        return $this;
    }

    public function removeDetailCommande(DetailCommande $detailCommande): self
    {
        if ($this->detailCommandes->contains($detailCommande)) {
            $this->detailCommandes->removeElement($detailCommande);
            // set the owning side to null (unless already changed)
            if ($detailCommande->getCommande() === $this) {
                $detailCommande->setCommande(null);
            }
            $this->subSomme($detailCommande->getSomme()) ;
        }

        return $this;
    }


    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|CommandeRestaurant[]
     */
    public function getCommandeRestaurants(): Collection
    {
        return $this->commandeRestaurants;
    }

    public function addCommandeRestaurant(CommandeRestaurant $commandeRestaurant): self
    {
        $cR = $this->getCommandeRestaurants() ;
        foreach ($cR as $c) {
            if($c->getRestaurant()->getId() == $commandeRestaurant->getRestaurant()->getId()) {
                $c->addSomme($commandeRestaurant->getSomme()) ;
                $c->addTime($commandeRestaurant->getTotalTemps()) ;
                return $this;
            }
        }
        $commandeRestaurant->setCommande($this) ;
        $this->commandeRestaurants[] = $commandeRestaurant;

        return $this;
    }

    public function removeCommandeRestaurant(CommandeRestaurant $commandeRestaurant): self
    {
        if ($this->commandeRestaurants->contains($commandeRestaurant)) {
            $this->commandeRestaurants->removeElement($commandeRestaurant);
            // set the owning side to null (unless already changed)
            if ($commandeRestaurant->getCommande() === $this) {
                $commandeRestaurant->setCommande(null);
            }
        }

        return $this;
    }

    public function getDateCommande(): ?\DateTimeInterface
    {
        return $this->dateCommande;
    }

    public function setDateCommande(?\DateTimeInterface $dateCommande): self
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    public function getSomme()
    {
        return $this->somme;
    }

    public function setSomme($somme): self
    {
        $this->somme = $somme;

        return $this;
    }
}
