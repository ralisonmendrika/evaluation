<?php

namespace App\Repository;

use App\Entity\CommandeRestaurant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CommandeRestaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommandeRestaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommandeRestaurant[]    findAll()
 * @method CommandeRestaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRestaurantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CommandeRestaurant::class);
    }

    public function findEvolutionCommande(){
        $sql = 'SELECT DAY(c.date_commande) jour,MONTH(c.date_commande) mois,YEAR(c.date_commande) an, CONCAT(CONVERT(DAY(c.date_commande),char),"/",CONVERT(MONTH(c.date_commande),char),"/",CONVERT(YEAR(c.date_commande),char)) ,count(*) nombreCommande FROM commande_restaurant JOIN commande c on commande_restaurant.commande_id = c.id JOIN restaurant r
  on commande_restaurant.restaurant_id = r.id WHERE c.date_commande IS NOT NULL GROUP BY YEAR(c.date_commande),MONTH(c.date_commande),DAY(c.date_commande) ORDER BY an,mois,jour';
        $conn = $this->getEntityManager()->getConnection() ;
        $stmt = $conn->prepare($sql) ;
        $stmt->execute() ;
        return $stmt->fetchAll() ;
    }

    public function findTempsMoyenPrep(){
        $sql = "SELECT r.nom nom,AVG(TIMESTAMPDIFF(MINUTE ,c.date_commande,commande_restaurant.date_livraison)) moyen FROM commande_restaurant JOIN commande c on commande_restaurant.commande_id = c.id JOIN restaurant r on commande_restaurant.restaurant_id = r.id WHERE commande_restaurant.date_livraison is not null AND c.date_commande IS NOT null GROUP BY  commande_restaurant.restaurant_id ORDER BY moyen DESC";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql) ;
        $stmt->execute() ;
        return $stmt->fetchAll();
    }

    public function findTopRestaurant($nombre=null) {
        //Select    from commande_restaurant c join restaurant r on c.restaurant = r.id join c.commande
        $sql = "SELECT r.nom nom,SUM(c.somme) chiffre FROM commande_restaurant c 
                  JOIN restaurant r on c.restaurant_id = r.id 
                  JOIN commande c2 on c.commande_id = c2.id 
                  GROUP BY r.id ORDER BY chiffre DESC" ;
        if($nombre!=null) {
            $sql = $sql." LIMIT ".$nombre." OFFSET 0" ;
        }
        $conn = $this->getEntityManager()->getConnection() ;
        $stmt = $conn->prepare($sql) ;
        $stmt->execute()  ;
        return $stmt->fetchAll() ;
    }

    public function getCommandeClient($user){
        $qb = $this->createQueryBuilder('c')
                  ->innerJoin('c.restaurant','r')
                    ->addSelect('r')
                      ->innerJoin('c.commande','co')
                    ->addSelect('co')
                    ->where('co.client = :user')
                    ->setParameter('user',$user)
                    ->orderBy('co.id','DESC') ;
            return $qb->getQuery();
    }

    public function getBaggagesLivreur(User $livreur) {
        $qb = $this->createQueryBuilder('c')
                    ->andWhere('c.etat = :etat')
                    ->setParameter('etat',CommandeRestaurant::RESERVE)
                    ->andWhere('c.livreur = :livreur')
                    ->setParameter('livreur',$livreur) ;
        return $qb->getQuery()->getResult();
    }

    public function getQueryCommande($etat=null,$restaurant=null,$livreur=null){
        $qb =  $this->createQueryBuilder('c')
                    ->innerJoin('c.commande','co')
                    ->addSelect('co')
                    ->innerJoin('co.client','cl')
                    ->addSelect('cl')
                    ->innerJoin('c.restaurant','r')
                    ->leftJoin('c.livreur','l')
                    ->addSelect('l')
                    ->addSelect('r') ;
        if($restaurant!=null) {
            $qb->andWhere('c.restaurant = :restaurantId')
                ->setParameter('restaurantId',$restaurant);
        }
        if($livreur!=null) {
            $qb->andWhere('c.livreur = :livreur')
                ->setParameter('livreur',$livreur) ;
        }
        if($etat!=null) {
            $qb->andWhere('c.etat = :etat')
                ->setParameter('etat', $etat);
        }
        $qb->orderBy('co.id','DESC') ;
        return $qb->getQuery() ;
    }

    // /**
    //  * @return CommandeRestaurant[] Returns an array of CommandeRestaurant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommandeRestaurant
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
