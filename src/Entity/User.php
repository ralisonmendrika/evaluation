<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    private $plainPassword ;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="client")
     */
    private $commandes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cleActivation;



    private $role  ;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Restaurant", mappedBy="responsable", cascade={"persist", "remove"})
     */
    private $restaurant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CommandeRestaurant", mappedBy="livreur")
     */
    private $commandeRestaurants;



    public function __construct()
    {
        $this->commandes = new ArrayCollection();
        $this->commandeRestaurants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function addRoles($roles) {
        $this->roles[] = $roles ;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER

        return $roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setClient($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getClient() === $this) {
                $commande->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getIsActif(): ?bool
    {
        return $this->isActif;
    }

    public function setIsActif(bool $isActif): self
    {
        $this->isActif = $isActif;

        return $this;
    }

    public function getCleActivation(): ?string
    {
        return $this->cleActivation;
    }

    public function setCleActivation(?string $cleActivation): self
    {
        $this->cleActivation = $cleActivation;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        // set (or unset) the owning side of the relation if necessary
        $newResponsable = $restaurant === null ? null : $this;
        if ($newResponsable !== $restaurant->getResponsable()) {
            $restaurant->setResponsable($newResponsable);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    public function simpleRole() {
        $roles = $this->getRoles() ;
        $role =  $roles[0] ;
        switch ($role){
            case "ROLE_RESTAURATEUR":
                return "RESTAURATEUR";
            case "ROLE_CLIENT":
                return "CLIENT";
            case "ROLE_ADMIN":
                return "Admin" ;
            case "ROLE_LIVREUR":
                return "Livreur";
        }
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|CommandeRestaurant[]
     */
    public function getCommandeRestaurants(): Collection
    {
        return $this->commandeRestaurants;
    }

    public function addCommandeRestaurant(CommandeRestaurant $commandeRestaurant): self
    {
        if (!$this->commandeRestaurants->contains($commandeRestaurant)) {
            $this->commandeRestaurants[] = $commandeRestaurant;
            $commandeRestaurant->setLivreur($this);
        }

        return $this;
    }

    public function removeCommandeRestaurant(CommandeRestaurant $commandeRestaurant): self
    {
        if ($this->commandeRestaurants->contains($commandeRestaurant)) {
            $this->commandeRestaurants->removeElement($commandeRestaurant);
            // set the owning side to null (unless already changed)
            if ($commandeRestaurant->getLivreur() === $this) {
                $commandeRestaurant->setLivreur(null);
            }
        }

        return $this;
    }

    

}
