<?php

namespace App\Entity;

use Beelab\TagBundle\Tag\TaggableInterface;
use Beelab\TagBundle\Tag\TagInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\PlatRepository")
 */
class Plat implements TaggableInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     */
    protected $tags ;

    /**
     * @Assert\NotBlank
     */
    private $tagsText;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *     max = 255 ,
     *     maxMessage = "Le nom ne doit pas excéder de  {{ limit }} caractères"
     *  )
     */
    private $nom;

    /**
     * @ORM\Column(type="decimal", precision=14, scale=2)
     * @Assert\NotBlank
     * @Assert\GreaterThan(
     *      value = 100 ,
     *      message = "Le prix doit être supérieur à 100 Ar"
     * )
     */
    private $prix;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Assert\NotBlank
     * @Assert\GreaterThan(
     *      value = 1,
     *     message = "Le prix doit être supérieur à 100 Ar"
     * )
     */
    private $temps;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Restaurant", inversedBy="plats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @Gedmo\Slug(fields={"id", "nom"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug ;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailCommande", mappedBy="plat")
     */
    private $detailCommandes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Photo", inversedBy="plat", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $photo;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateTagAt;

    public function __construct()
    {
        $this->detailCommandes = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->updateTagAt = new \DateTimeImmutable() ;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {

        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix()
    {
        return $this->prix;
    }

    public function setPrix($prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getTemps()
    {
        return $this->temps;
    }

    public function setTemps($temps): self
    {
        $this->temps = $temps;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * @return Collection|DetailCommande[]
     */
    public function getDetailCommandes(): Collection
    {
        return $this->detailCommandes;
    }

    public function addDetailCommande(DetailCommande $detailCommande): self
    {
        if (!$this->detailCommandes->contains($detailCommande)) {
            $this->detailCommandes[] = $detailCommande;
            $detailCommande->setPlat($this);
        }
        $this->updateTagAt = new \DateTimeImmutable() ;

        return $this;
    }

    public function removeDetailCommande(DetailCommande $detailCommande): self
    {
        if ($this->detailCommandes->contains($detailCommande)) {
            $this->detailCommandes->removeElement($detailCommande);
            // set the owning side to null (unless already changed)
            if ($detailCommande->getPlat() === $this) {
                $detailCommande->setPlat(null);
            }
        }
        $this->updateTagAt = new \DateTimeImmutable() ;

        return $this;
    }

    public function getPhoto(): ?Photo
    {
        return $this->photo;
    }

    public function setPhoto(?Photo $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function addTag(TagInterface $tag): void
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }
    }

    public function removeTag(TagInterface $tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function hasTag(TagInterface $tag): bool
    {
        return $this->tags->contains($tag);
    }

    public function getTags(): iterable
    {
        return $this->tags;
    }

    public function getTagNames(): array
    {
        return empty($this->tagsText) ? [] : \array_map('trim', explode(',', $this->tagsText));
    }

    public function setTagsText(?string $tagsText): void
    {
        $this->tagsText = $tagsText;
        $tags = explode(',',$this->tagsText) ;
        $tagObj=null ;
        foreach ($tags as $tag) {
            if(empty($tag)) {
                $tagObj = new Tag();
                $tagObj->setName(trim($tag)) ;
                $this->addTag($tagObj) ;
            }
        }
        $this->updateTagAt = new \DateTimeImmutable();
    }

    public function getTagsText(): ?string
    {
        $this->tagsText = \implode(', ', $this->tags->toArray());

        return $this->tagsText;
    }

    public function getUpdateTagAt(): ?\DateTimeInterface
    {
        return $this->updateTagAt;
    }

    public function setUpdateTagAt(?\DateTimeInterface $updateTagAt): self
    {
        $this->updateTagAt = $updateTagAt;

        return $this;
    }


}
