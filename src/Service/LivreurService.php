<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 22:56
 */

namespace App\Service;


use App\Entity\CommandeRestaurant;
use App\Entity\User;
use App\Repository\CommandeRestaurantRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;

class LivreurService
{
    private $em ;

    private $security;

    private $crRep ;

    public function __construct(ObjectManager $objectManager,Security $security,CommandeRestaurantRepository $commandeRestaurantRepository)
    {
        $this->em = $objectManager ;
        $this->security = $security;
        $this->crRep = $commandeRestaurantRepository ;
    }

    public function reserver(CommandeRestaurant $commandeRestaurant) {
        $commandesRestaurant = $this->crRep->getBaggagesLivreur($this->security->getUser()) ;
        if(count($commandesRestaurant)>=2) {
            return false ;
        }
        $commandeRestaurant->reserver($this->security->getUser()) ;
        $this->em->flush();
        return true ;
    }

    public function recupere(CommandeRestaurant $commandeRestaurant,User $user)
    {
        if($commandeRestaurant->getLivreur()->getId()!=$user->getId()) {
            return false ;
        }
        $commandeRestaurant->recuperer();
        $this->em->flush() ;
        return true ;
    }
}