<?php

namespace App\DataFixtures;

use App\Entity\Photo;
use App\Entity\Plat;
use App\Entity\Restaurant;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RestaurantFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $tag = new Tag() ;
        $tag->setName("Entrée") ;
        $tags = [] ;
        $tags[] = $tag ;
        $manager->persist($tag);
        $tag = new Tag() ;
        $tag->setName("Plat Malagasy") ;
        $tags[] = $tag ;
        $manager->persist($tag) ;
        $nomRestaurant = "Restaurant" ;
        $plats     = ["Plat 1","Plat 2","Plat 5","Plat 6"] ;
        $prix      = [5800,3800,2800,10000,5000] ;
        $temps     = [10,20,30,5] ;
        $localisation  = ["Antaninarenina","Antanimena","Tanjombato"] ;
        $rest =null ;

        for($i=1;$i<=10;$i++) {
            $rest = new Restaurant() ;
            $rest->setNom($nomRestaurant.$i)  ;
            $rest->setLocalisation($localisation[$i%3]) ;
            for($p=0;$p<count($plats);$p++){
                $platOb = new Plat() ;
                if($p%2==0){
                    $platOb->addTag($tags[0]) ;
                }else {
                    $platOb->addTag($tags[0]) ;
                    $platOb->addTag($tags[1]) ;
                }
                $platOb->setNom($plats[$p]) ;
                $platOb->setPrix($prix[$p%5]) ;
                $platOb->setTemps($temps[$p%4]) ;
                $platOb->setDescription("Description du truc") ;
                $rest->addPlat($platOb) ;
                $photo = new Photo() ;
                $photo->setImageName(str_replace(" ","",$platOb->getNom()).'.jpg') ;
                $photo->setAlt('Description image'. $platOb->getNom()) ;
                $photo->setUpdateAt(new \DateTimeImmutable()) ;
                $platOb->setPhoto($photo) ;

            }
            $manager->persist($rest) ;
        }

        $manager->flush();
    }
}
