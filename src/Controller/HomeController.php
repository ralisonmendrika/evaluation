<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\DetailCommande;
use App\Entity\Plat;
use App\Entity\Restaurant;
use App\Form\DetailCommandeType;
use App\Service\PanierService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/restaurant", name="restaurant")
     */
    public function index(Request $request,PaginatorInterface $paginator)
    {
        $restaurantRep = $this->getDoctrine()->getRepository(Restaurant::class) ;
        $pagination = $paginator->paginate(
           $restaurantRep->getQueryFindAll(),
           $request->query->getInt('page',1),
            5
        );
        return $this->render('home/index.html.twig',['restaurants'=>$pagination]);
    }

    /**
     * @Route("/restaurant/{slug}",name="plat_restaurant")
     */
    public function getPlatRestaurant(Request $request,PaginatorInterface $paginator,$slug=null){
        if($slug==null) {
            return $this->redirectToRoute('restaurant') ;
        }
        $restaurantRep = $this->getDoctrine()->getRepository(Restaurant::class) ;
        $platRep       = $this->getDoctrine()->getRepository(Plat::class) ;
        $restaurant    = $restaurantRep->findOneBy(["slug"=>$slug]) ;
        if(!$restaurant) {
            throw $this->createNotFoundException('Restaurant Introuvable') ;
        }
        $pagination = $paginator->paginate(
            $platRep->getQueryPlatByRestaurant($restaurant),
            $request->query->getInt('page',1),
            5
        ) ;
        return $this->render('home/restaurant.html.twig',['restaurant'=>$restaurant,'plats'=>$pagination]);
    }

    /**
     * @Route("/plat/{slug}",name="detail_plat")
     */
    public function getDetailPlat(PanierService $panierService,Request $request,PaginatorInterface $paginator,$slug=null){
        if($slug==null) {
             return $this->redirectToRoute('restaurant') ;
        }
        $platRep       = $this->getDoctrine()->getRepository(Plat::class) ;
        $plat    = $platRep->findOneBy(["slug"=>$slug]) ;
        if(!$plat) {
            throw $this->createNotFoundException('Plat Introuvable') ;
        }
        $detailCommande = new DetailCommande();
        $detailCommande->setPlat($plat) ;
        $form           = $this->createForm(DetailCommandeType::class,$detailCommande) ;
        $form->handleRequest($request) ;
        if($form->isSubmitted() && $form->isValid()) {
            $this->denyAccessUnlessGranted('ROLE_CLIENT') ;
            $panierService->addPanier($detailCommande) ;
            return $this->redirectToRoute('detail_plat',['slug'=>$slug])  ;
        }
        return $this->render('home/detailPlat.html.twig',['plat'=>$plat,'form'=>$form->createView()]);
    }


}
