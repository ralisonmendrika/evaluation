<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandeRestaurantRepository")
 */
class CommandeRestaurant
{
    const PANIER = 0 ;
    const VALIDE = 1 ;
    const PRET   = 2 ;
    const RESERVE = 3 ;
    const RECUPERE = 4 ;
    const LIVRE    = 5 ;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Commande", inversedBy="commandeRestaurants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commande;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Restaurant", inversedBy="commandeRestaurants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=2)
     */
    private $somme;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commandeRestaurants")
     */
    private $livreur;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateReservation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateRecuperation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateLivraison;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datePret;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalTemps;



    public function __construct()
    {
        $this->baggageLivraisons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommande(): ?Commande
    {
        return $this->commande;
    }

    public function setCommande(?Commande $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getSomme()
    {
        return $this->somme;
    }

    public function setSomme($somme): self
    {
        $this->somme = $somme;

        return $this;
    }

    public function addSomme($value) {
        $this->somme += $value ;
    }

    public function subSomme($value){
        $this->somme -= $value ;
    }

    public function addTime($value) {
        $this->totalTemps += $value  ;
    }

    public function subTime($value) {
        $this->totalTemps -= $value ;
    }

    public function getLivreur(): ?User
    {
        return $this->livreur;
    }

    public function setLivreur(?User $livreur): self
    {
        $this->livreur = $livreur;

        return $this;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->dateReservation;
    }

    public function setDateReservation(?\DateTimeInterface $dateReservation): self
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    public function reserver(User $livreur) {
        $this->setEtat(CommandeRestaurant::RESERVE);
        $this->setLivreur($livreur) ;
        $this->setDateReservation(new \DateTimeImmutable()) ;
    }

    public function recuperer() {
        $this->setEtat(CommandeRestaurant::RECUPERE) ;
        $this->setDateRecuperation(new \DateTimeImmutable());
    }

    public function getDateRecuperation(): ?\DateTimeInterface
    {
        return $this->dateRecuperation;
    }

    public function setDateRecuperation(?\DateTimeInterface $dateRecuperation): self
    {
        $this->dateRecuperation = $dateRecuperation;

        return $this;
    }

    public function traduireEtat(){
        switch ($this->getEtat()){
            case CommandeRestaurant::VALIDE :
                return "En cours" ;
            case CommandeRestaurant::PRET :
                return "Prête";
            case CommandeRestaurant::RESERVE :
                return "Résérvée";
            case CommandeRestaurant::RECUPERE :
                return "Récupérée";
            case CommandeRestaurant::LIVRE :
                return "Livrée" ;
            case CommandeRestaurant::PANIER :
                return "Panier client";
        }
    }

    public function declarerLivre(){
        $this->setEtat(CommandeRestaurant::LIVRE) ;
        $this->dateLivraison = new \DateTimeImmutable() ;
    }

    public function getDateLivraison(): ?\DateTimeInterface
    {
        return $this->dateLivraison;
    }

    public function setDateLivraison(?\DateTimeInterface $dateLivraison): self
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    public function getDatePret(): ?\DateTimeInterface
    {
        return $this->datePret;
    }

    public function setDatePret(?\DateTimeInterface $datePret): self
    {
        $this->datePret = $datePret;

        return $this;
    }

    public function getTotalTemps(): ?int
    {
        return $this->totalTemps;
    }

    public function setTotalTemps(int $totalTemps): self
    {
        $this->totalTemps = $totalTemps;

        return $this;
    }
}
