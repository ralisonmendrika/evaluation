<?php

namespace App\Controller;

use App\Entity\CommandeRestaurant;
use App\Repository\CommandeRestaurantRepository;
use App\Service\LivreurService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/livreur")
 */
class LivreurController extends AbstractController
{
    /**
     * @Route("/", name="livreur_attente_livraison")
     */
    public function index(Request $request,PaginatorInterface $paginator,CommandeRestaurantRepository $commandeRestaurantRepository)
    {
        $commandes = $paginator->paginate(
            $commandeRestaurantRepository->getQueryCommande(CommandeRestaurant::PRET),
            $request->query->getInt('page',1),
            5
        );
        return $this->render('admin/index.html.twig', [
            'commandes' => $commandes,
            'content'     => "admin/_attente.html.twig",
            'title' => "Commande en attente livraison"
        ]);
    }


    /**
     * @Route("/reserver/{idCommandeResto}", name="livreur_reserver_livraison")
     */
    public function reserver($idCommandeResto,LivreurService $livreurService)
    {
        $commandeRest = $this->getDoctrine()->getRepository(CommandeRestaurant::class)->find($idCommandeResto);
        if(!$commandeRest){
            throw $this->createNotFoundException('Commande Introuvable') ;
        }
        if(!$livreurService->reserver($commandeRest)){
            $this->addFlash('type','danger');
            $this->addFlash('message','Vous ne pouvez pas resever plus de 2 commandes') ;
        }else{
            $this->addFlash('type','success');
            $this->addFlash('message','Résérvation réussie') ;
        }
        return $this->redirectToRoute('livreur_attente_livraison') ;


    }

    /**
     * @Route("/recuperer", name="livreur_recuperer_livraison")
     */
    public function recuperer(Request $request,PaginatorInterface $paginator,CommandeRestaurantRepository $commandeRestaurantRepository)
    {
        $commandes = $paginator->paginate(
            $commandeRestaurantRepository->getQueryCommande(CommandeRestaurant::RESERVE,null,$this->getUser()),
            $request->query->getInt('page',1),
            5
        );
        return $this->render('admin/index.html.twig', [
            'commandes' => $commandes,
            'content'=>'admin/_recuperation.html.twig',
            'title'=>'Commandes résérvées'
        ]);
    }

    /**
     * @Route("/recup/{id}",name="livreur_recup_livraison")
     */
    public function recup($id,LivreurService $livreurService){
        $commandeRest = $this->getDoctrine()->getRepository(CommandeRestaurant::class)->find($id);
        if(!$commandeRest){
            throw $this->createNotFoundException('Commande Introuvable') ;
        }
        if(!$livreurService->recupere($commandeRest,$this->getUser())){
            $this->addFlash('type','danger');
            $this->addFlash('message','Vous ne pouvez pas recuperer  cette commande') ;
        }else{
            $this->addFlash('type','success');
            $this->addFlash('message','Commande récupérée') ;
        }
        return $this->redirectToRoute('livreur_recuperer_livraison') ;
    }

}
