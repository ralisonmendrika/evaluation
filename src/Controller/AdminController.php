<?php

namespace App\Controller;

use App\Repository\CommandeRepository;
use App\Repository\CommandeRestaurantRepository;
use App\Service\PanierService;
use Dompdf\Dompdf;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/commande", name="commande_en_cours")
     */
    public function index(Request $request,CommandeRestaurantRepository $commandeRestaurantRepository,PaginatorInterface $paginator)
    {
        $query = $commandeRestaurantRepository->getQueryCommande(1)  ;
        $commandes = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            5
        );
        return $this->render('admin/index.html.twig', [
            'commandes'=>$commandes,
            'title'=>'Commandes en cours',
            'content'=>'admin/_aucune.html.twig'
        ]);
    }

    /**
     * @Route("/suivi/commande",name="suivi_commande")
     */
    public function suiviCommande (Request $request,CommandeRestaurantRepository $commandeRestaurantRepository,PaginatorInterface $paginator) {
        $query = $commandeRestaurantRepository->getQueryCommande() ;
        $commandes = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            5
        );
        return $this->render('admin/suivi.html.twig',['commandes'=>$commandes]) ;
    }


    /**
     * @Route("/dashboard",name="admin_dashboard")
     */
    public function dashboard(CommandeRestaurantRepository $commandeRestaurantRepository){
        $topFive = $commandeRestaurantRepository->findTopRestaurant(5) ;
        $tempsMoyen = $commandeRestaurantRepository->findTempsMoyenPrep() ;
        return $this->render("admin/dashboard.html.twig",['topFive'=>$topFive,'tempsMoyen'=>$tempsMoyen]) ;
    }

    /**
     * @Route("/chiffre/pdf",name="admin_export_pdf")
     */
    public function exporterPdf(CommandeRestaurantRepository $commandeRestaurantRepository,PanierService $panierService){
        $commandes = $commandeRestaurantRepository->findTopRestaurant() ;
        $commandes = $panierService->addpourcentage($commandes) ;
        $dompdf = new Dompdf() ;
        $html   = $this->renderView("admin/chiffrepdf.html.twig",['commandes'=>$commandes])  ;
        $dompdf->setPaper('A4','Landscape') ;
        $dompdf->loadHtml($html) ;
        // Render the HTML as PDF
        $dompdf->render();
        $now = new \DateTimeImmutable() ;
        // Output the generated PDF to Browser (force download)
        $dompdf->stream("chiffre-affaire-".$now->format("dmyhi").".pdf", [
            "Attachment" => true
        ]);

    }


}
