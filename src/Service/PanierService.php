<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 07:47
 */

namespace App\Service;


use App\Entity\Commande;
use App\Entity\CommandeRestaurant;
use App\Entity\DetailCommande;
use App\Entity\Plat;
use App\Repository\DetailCommandeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class PanierService
{

    private $em ;
    private $sec ;

    public function __construct(ObjectManager $em,Security $security)
    {
        $this->em = $em ;
        $this->sec = $security ;
    }

    public function recuperer(CommandeRestaurant $commandeRestaurant) {
        if($commandeRestaurant->getCommande()->getClient()->getId() != $this->sec->getUser()->getId()) {
            return false ;
        }
        $commandeRestaurant->declarerLivre() ;
        $this->em->flush();
        return true ;

    }

    public function validerCommande(){
        $commande = $this->getOrCreatePanier() ;
        $commandeRestaurants = $this->em->getRepository(CommandeRestaurant::class)->findBy(['commande'=>$commande]) ;
        if(count($commandeRestaurants)<=0){
            return;
        }
        $commande->setEtat(Commande::VALIDE) ;
        foreach ($commandeRestaurants as $c) {
            $c->setEtat(CommandeRestaurant::VALIDE) ;
        }
        $commande->setDateCommande(new \DateTimeImmutable()) ;
        $this->em->flush();
    }

    public function deleteDetailCommande($idDetailCommande) {
        $commande = $this->getOrCreatePanier() ;
        $detailComm = $this->em->getRepository(DetailCommande::class)->find($idDetailCommande) ;
        if(!$detailComm || $detailComm->getCommande()->getEtat()==Commande::VALIDE || $commande->getId()!=$detailComm->getCommande()->getId()){
            throw new NotFoundHttpException("Detail Commande introuvable") ;
        }
        $commande->removeDetailCommande($detailComm)  ;
        $commadeRestaurant = $this->em->getRepository(CommandeRestaurant::class)->findOneBy(['restaurant'=>$detailComm->getPlat()->getRestaurant()
        ,'commande'=>$commande]) ;
        $commadeRestaurant->subSomme($detailComm->getSomme()) ;
        $commadeRestaurant->subTime($detailComm->getTempsTotal()) ;
        if($commadeRestaurant->getSomme()<=0){
            $commande->removeCommandeRestaurant($commadeRestaurant) ;
            $this->em->remove($commadeRestaurant)  ;
        }
        $this->em->remove($detailComm) ;
        $this->em->flush();
    }

    public function getOrCreatePanier() : Commande{
        $security = $this->sec ;
        if(!$security->isGranted("ROLE_CLIENT")) {
            throw new AccessDeniedException() ;
        }
        $commandeRepository = $this->em->getRepository(Commande::class) ;
        $commande = $commandeRepository->findOneBy(['etat'=>0,'client'=>$security->getUser()]) ;
        if(!$commande){
            $commande = new Commande();
            $commande->setSomme(0);
            $commande->setEtat(0) ;
            $commande->setClient($security->getUser()) ;
            $this->em->persist($commande) ;
            $this->em->flush() ;
        }
        return $commande ;
    }

    public function addPanier(DetailCommande $detailCommande) {
        $commande = $this->getOrCreatePanier();
        $detailCommande->setPrix($detailCommande->getPlat()->getPrix()) ;
        $commande->addDetailCommande($detailCommande) ;
        $commandeRestaurant = new CommandeRestaurant() ;
        $commandeRestaurant->setEtat(0);
        $commandeRestaurant->setSomme($detailCommande->getSomme()) ;
        $commandeRestaurant->setTotalTemps($detailCommande->getTempsTotal()) ;
        $commandeRestaurant->setRestaurant($detailCommande->getPlat()->getRestaurant()) ;
        $commande->addCommandeRestaurant($commandeRestaurant) ;
        $this->em->flush();
    }

    public function recommender(CommandeRestaurant $restaurant) {
     //   $commande = $this->getOrCreatePanier() ;
        $detailsCommande = $restaurant->getCommande()->getDetailCommandes() ;

        foreach ($detailsCommande as $d) {
            $detailCommande = new DetailCommande() ;
            $detailCommande->setPlat($d->getPlat());
            $detailCommande->setNombre($d->getNombre()) ;
            $this->addPanier($detailCommande) ;
        }

    }

    public function addpourcentage(array $commandes)
    {
        $result = [] ;
        $arrayTmep = [] ;
        $total = $this->total($commandes) ;
        foreach ($commandes as $r) {
            $arrayTmep['nom'] = $r['nom'] ;
            $arrayTmep['chiffre'] = $r['chiffre'] ;
            $arrayTmep['pourcentage'] = $this->getPourcentage((int)$r['chiffre'],$total) ;
            $result[] = $arrayTmep ;
        }
        return $result ;
    }

    private function total(array $commande) {
        $total = 0 ;
        foreach ($commande as $r) {
            $total += $r['chiffre'] ;
        }
        return $total ;
    }

    private function getPourcentage($chiffre,$total) {
        return ($chiffre*100)/$total ;
    }




}